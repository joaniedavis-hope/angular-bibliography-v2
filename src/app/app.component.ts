import { Component } from '@angular/core';
import { Bibentry } from './models/bibentry.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  {
  title = "CSCI 495 Bibliography App";
  
  constructor () {

  }
}
